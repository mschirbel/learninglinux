# File Systems

## Creation

Existem duas categorias:
    - non-journaling: não guarda mudanças - ex: ext2
    - journaling: guarda mudanças que ainda não foram escritas no FS - ex: XFS, ext4

Para criar um FS usamos o comando `mkfs`.

```
mkfs -t <FS-TYPE> /dev/sda1
mkfs -t ext4 /dev/sda1
mkfs.<FS-TYPE> /dev/sda1
mkfs.ext4 /dev/sda1

mkfs -t ext4 -L LABEL_TEST /dev/sda1
```
*sim, as duas são aceitas*.

Para ver os FS use o comando `blkid`.

```
blkid /dev/sda1
```

### Btrfs

Usa o CoW(Copy on Write), sempre que uma escrita ocorrer, é feita uma cópia do arquivo para que somente as mudanças sejam escritas no arquivo original(melhor performance e fácil de fazer rollback).

Ele usa subvolumes que podem ser acessados que quaisquer diretórios. Cada subvolume pode desempenhar uma task diferente.

Outra *feature* é o snapshot. Que o subvolume guarda os metadados e estruturas de diretórios.

### FAT

File Allocation Table. Fat é usado pelo EFI boot. Com o Fat temos a chance de usar nomes longos para arquivos.

## Disk Space

O comando principal para sabermos o quando do disco está ocupado é o `df`. Ele mostra todos os mount points do file system.

```
df -h /
```

Outro comando útil é o `du`. Este mostra a quantidade que está sendo usada no disco a partir de um certo diretório

```
du -sh .
```

###### inode

inodes são partes fundamentais de um file system. Eles guardam informações sobre permissões, donos, e tipos de arquivos. E cada file system tem um limite para inodes.

Para verificar os inodes podemos usar os comandos

```
df -i
du --inodes
ls -i
```

## Maintenance

Podemos fazer um check do file system com o comando `fsck`.

```
fsck -r LABEL=SRV
fsck /dev/sda1
```

Se o file system está mounted, não seria bom alterá-lo pois podemos corromper dados.
No arquivo `/etc/fstab` tenha certeza que a última coloca do `/` está com um `1` marcado e que o `/boot` está com um `2`. Isso indica que o `/` será o primeiro file system a ser verificado e o `/boot` o segundo.

Veja que no `swap` está com `0` pois não precisamos verificar por consistência de dados.

---

Para file systems com *ext2*, *ext3* ou *ext4* podemos usar o comando `e2fsck`.

As configurações do `mke2fs` está em `/etc/mke2fs.conf`. Aqui fica aquele *defaults* que vemos no `/etc/fstab`.

```
mke2fs -t ext4 -L EXTRA /dev/sdb1
```

Para fazer alterações no file system podemos usar o comando `tune2fs`.
```
tune2fs -i 3w /dev/sdb1 # trocar o check no fs
tune2fs -l /dev/sdb1 # listar as configs
```

---

Para file systems com *xfs* podemos usar o comando `xfs_repair`. Para realizar um defragmentação do file system podemos usar o comando `xfs_fsr`. 

Para fazer um debug, `xfs_db`.

## Hierarquia

O file system deve:
    - organizar os dados
    - persistir os dados
    - garantir a integridade dos dados
    - ser de fácil recuperação dos dados

Para isso, foi criada um padrão para os file systems, o ![FHS](http:www.pathname.com/fhs).

No caso, o padrão manda que:

1. temos uma árvore invertida, com o root sendo a raíz
2. um `.` representa o diretório atual
3. um `..` representa o diretório acima na árvore
4. qualquer arquivo com um `.` no começo do nome será um arquivo escondido
5. arquivos são case sensitive
6. usamos `/` para delimitar diretórios

### Diretórios principais

`/bin`: arquivos executáveis que usuários regulares podem usar. Comandos como `ls, mkidr, pwd`.

`/boot`: arquivos para bootar o sistema, o Kernel também está aqui

`/dev`: onde estão referenciados quaisquer drivers e hardwares conectados no sistema

`/etc`: arquivos de configuração para o sistema

`/home`: onde qualquer usuário regular tem a sua pasta

`/lib`: arquivos de libraries para aplicações em 32bits

`/lib64`: arquivos de libraries para aplicações em 64bits

`/media`: onde CD,DVD podem ser montados

`/mnt`: onde podemos acessar hardwares conectados, como pendrives

`/opt`: diretório opcionais

`/proc`: informações sobre o Linux rodando

`/root`: home do root

`/run`: arquivos necessários para o systemd(algo novo pro linux). Era o antigo `/var/run`;

`/sbin`: onde o adminstrador do sistema tem as ferramentas de administração

`/srv`: usado para aplicações de servidores

`/sys`: hardware do sistema

`/tmp`: aplicações usarem arquivos temporários

`/usr`: tem mais aplicações e comandos externos ao nativos do Linux

`/var`: arquivos que variam em tamanho, como logs, por exemplo.