# Symbolic Link

Links simbólicos são como atalhos no Windows.

Para criar um, usamos o comando `ln`. Esse é um *hard link*, ou seja, esse link só funciona se estiver no mesmo file system do que o arquivo original.

```
ln -s text.txt /opt/shortcut.txt
```

O primeiro argumento é o arquivo original e o segundo é o nome do link.

Podemos usar também o `ln -s` que cria um *soft link* que pode funcionar entre diversos file systems.

Para desfazer o link simbólico use o comando `unlink`.

```
unlink /opt/shortcut.txt
```

Se editarmos o arquivo do link simbólico, editaremos também o original.
Se movermos o arquivo original, será criado um novo arquivo.