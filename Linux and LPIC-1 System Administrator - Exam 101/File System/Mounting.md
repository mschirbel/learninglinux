# Mount

O `mount` é uma indicação para um disco estar correspondido com um diretório no seu file system.

Como um navio atracando em um porto. Assim que um arquivo chega no diretório, ele repassa essa informação para o device físico:

![](../media/mount.PNG)

Assim, da mesma forma com o `unmount`.

![](../media/unmount.PNG)

Para fazer isso permanente, devemos ter uma entrada desse mount point no `/etc/fstab`.

---

Se você rodar o comando `mount`, verá que ele possui o mesmo conteúdo do arquivo `/etc/mtab`. Que por sua vez é um link simbólico para o arquivo `/proc/mounts`.

Para o comando `mount` precisamos indicar um device e um diretório.

Para fazer um `mount` de tudo que está no `/etc/fstab` use o comando:
```
mount -a
```