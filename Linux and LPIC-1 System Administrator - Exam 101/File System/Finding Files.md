# Find Files

Um dos comandos que podemos usar para localizar arquivos é o `locate`.

```
locate passwd
```

Procura em um database local de arquivos e pastas. Acaba sendo um pouco mais rápido do que o `find`.
Entretanto, vc precisa fazer o update desse database. Tem que ser root para rodar esse comando.

```
sudo su -
updatedb
```

Com esse comando acima podemos fazer o update do `locate`. Esse comando é periódico e podemos ver as configurações em `/etc/update.conf`.

Para encontrar binários de comandos ou manuais, use o comando `whereis`.