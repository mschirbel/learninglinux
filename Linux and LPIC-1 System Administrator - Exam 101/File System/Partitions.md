# Partitions

## MBR

Usamos o comando `lsblk` para listar os block devices do sistema. Um block devide é qualquer hardware que tenha dados escritos em formato de bloco. Como HD's, por exemplo.

Para criar uma partição usamos o comando `fdisk` seguido pelo nome do devide que vamos trabalhar

```
fdisk /dev/sda
```

Agora estamos num shell próprio para trabalhar com partições.
Para criar uma partition table pressionamos a tecla `n`. Agora a letra `p` para indicar que é uma primária.
Depois inserimos qual o número da partition e a quantidade de bytes para cada setor.

E por último indicamos o tamanho da partition. Para ver as partition use a letra `p`. Para escrever no disco, use a tecla `w`.

Depois rode o comando `lsblk` e verá a partition nele. Ou podemos usar o comando `fdisk -l <DEVICE NAME>`.

---

Outra maneira de criar partitions é com o comando `parted`.

```
parted
p
mkpart # iniciar
primary # tipo de partition
ext2 # formatação do disco
1 # MB de start
1000 # MB de fim
p # printar
```

#### ID's

Existem alguns ID's importantes:

    - 83: padrão de FS Linux
    - 82: para swaps
    - 8e: LVM Volumes

## GPT

Com GPT temos uma nova forma de controlar as partitions. Para acessar o GPT use o comando `gdisk` ou o comando `parted`.

```
gdisk /dev/sda
```

É bem parecido com o `fdisk`. Seguindo os mesmos princípios de antes. A diferença é que podemos conter até 128 partitions dentro do GPT.

Outra diferença é que nao precisamos especificar o início e fim do disco, apenas o tamanho total.
Para escrever no disco use a tecla `w`.

Com o `parted`:

```
parted
mklabel gpt
optional
ext2
1
500
p
```

Para verificar os discos e partições use o comando `lsblk`.

## Swap

Swap é usada quando a RAM está quase cheia. O SO pega as apps que estão a mais tempo sem utilizar e começa a jogar para disco. Se a RAM encher, o sistema pode falhar e até ter dados corrompidos.

Podemos usar o *swap file*. Isso traz muitos problemas com performance.
Para isso, podemos usar uma *swap partition*. Ainda temos que escrever em disco, mas não precisamos ler de somente 1 só arquivo.

Para criar uma *swap partition* podemos usar alguns comandos. No caso, vamos usar o `gdisk`.

```shell
gdisk /dev/sda
p
n # nova partiion
<enter> # para o ID
<enter> # para o primeiro setor
<enter> # para o ultimo setor, no caso todo o disco
8200 # ID para swap
p
w # escrever no disco
y # confirmar
lsblk
```

Agora precisamos colocar um *swap file system* em cima dessa partition. Fazemos isso com o comando `mkswap`.

```
mkswap -L SWAP /dev/sda2
```

`-L` é um parâmetro para Label, que é uma boa prática. Mas ainda não estamos utilizando. Para usar, temos que ligar com o comando `swapon -U <UUID>` ou `swapon -L <LABEL>`. Para desligar `swapoff -a`.
O parâmetro `-a` indica tudo que está marcado como swap no `/etc/fstab`.

Verifique com o comando `free -m`.

Mas ainda temos um problema, sempre que ligarmos o computador, essa partition não vai estar ligada. Para isso, precisamo adicionar no arquivo `/etc/fstab`. File System Table File é o que o computador usa pra saber quais os file systems que precisa usar.

```
sudo vim /etc/fstab
```

Na primeira coluna temos o local físico do device ou o UUID. A segunda coluna indica o mount point. A terceira coluna indica o tipo de file system na partition. A quarta coluna indica quais opções podemos usar para fazer o mount. O próximo número indica o dumping de dados, geralmente é 0. O último número é para file system checking, para ver se todos os arquivos estão intactos.