# Access and Permissions

## Read Permissions

Podemos ver as permissões de um arquivo usando o comando `ls -l`.

```
ls -la
D:\Projects\kubernetes-projects› ls -la
total 16
drwxr-xr-x 1 Marcelo Schirbel 197121 0 Jan 13 20:52 .
drwxr-xr-x 1 Marcelo Schirbel 197121 0 Nov  2 12:51 ..
drwxr-xr-x 1 Marcelo Schirbel 197121 0 Jan 14 20:38 k8s-ansible-local
drwxr-xr-x 1 Marcelo Schirbel 197121 0 Jan 13 19:53 kubespray
```

O primeiro caractere indica o tipo de arquivo.

Os 9 seguintes são as permissões do arquivo. Os 3 primeiros são para o dono do arquivo. Os 3 seguintes para o grupo no qual o dono do arquivo está inserido e os 3 últimos para qualquer outro usuário.

As permissões se dão na seguinte forma:

```
r # read permission
w # write permission
x # execute permission
- # no permission
```

Na sua forma octal:

```
4 # read
2 # write
1 # execute
0 # nothing
```

Posterior às permissões, temos o usuário dono do arquivo, seguido pelo grupo ao qual está inserido.
Depois a última data de modificação do arquivo. E por último o nome do arquivo.

## Modify Permissions

O comando `chown` muda a titularidade do arquivo, seu dono:

```
chown marcelo teste.txt
```

Entretanto, podemos alterar o grupo também:
```
chown :grupo teste.txt
```

Ou os dois ao mesmo tempo
```
chown marcelo:grupo teste.txt
```

Para alterar somente o grupo, usamos o comando `chgrp`.

```
chgrp admin test.txt
```

O comando `chmod` troca as permissões do arquivo, seja em regra octal ou alfabética:

``` 
chmod 775 test.txt
chmod o-r test.txt # remove a permissão de leitura para outros.
```

## Advanced

#### SUID

Veja por exemplo o arquivo `/etc/passwd`.

```
ls -l /etc/passwd
```

Perceba que o arquivo tem o root como dono, mas ainda assim, cada usuário pode trocar a sua senha com um comando. E como isso funciona?

Olhe o arquivo do comando `passwd`:

```
ls -l /usr/bin/passwd
```

Perceba que tem alguma coisa diferente. Existe um `s`no lugar do `x` para o dono. Isso é o *SUID*, Set User ID bit. No caso, caso o usuário rode esse programa ele se passará como o dono do arquivo.
Mas isso não é tão útil pois não funciona direto hoje, por questões de segurança.

Para trocar o *SUID* usamos:

```
chmod 4760 arquivo.txt
# ou
chmod u+s arquivo.txt
```
Esse `4` é o número octal indicativo do *SUID*. Sendo o `760` as permissões para dono, grupo e outros.

Para remover use o `chmod 0760` ou `chmod u-s`

#### SGID

Olhe o arquivo do comando `write`:

```
ls -l /usr/bin/write
```

Perceba que tem alguma coisa diferente. Existe um `s`no lugar do `x` para o grupos. Isso é o *SGID*, Set Group ID bit. No caso, caso o grupo rode esse programa ele se passará como o grupo dono do arquivo.

Isso pode ser útil para diretórios, assim podemos dividir arquivos sem ficar nos preocupando com permissões individuais.

Para verificar os grupos use o comando `groups`.

Para trocar o *SGID* use:

```
chmod -R 2755 directory/
```

Esse `2` é o número octal indicativo do *SGID*. Sendo o `755` as permissões para dono, grupo e outros.

Para remover use o `chmod 0755` ou `chmod g-s`

#### Sticky Bit

Olhe o diretório `/tmp`

Nesse diretório temos um `t`. Esse `t` representa que somente permite o criador de um arquivo o remova.

Para trocar o *Sticky Bit* usamos:

```
chmod 1760 arquivo.txt
# ou
chmod o+t arquivo.txt
```

## Default

Quando rodamos o comando `umask` temos o retorno default do **umask**. Mas o que o *umask* faz?

```
umask
```

No Linux temos que os diretório tem permissão `777` quando criados e os arquivos `666`. O *umask* faz a subtração do valor pelo valor default dos diretórios e arquivos.

As configurações do *umask* estão:

    - /etc/bashrc : que é para todo o sistema
    - /home/user/.bashrc : que é para o usuário rodando o comando

Para usuários comuns, o valor do *umask* é 002. Para o *root* é de 022.

Para setar o *umask* usamos:

```
umask u=rwx,g=,o=
```

Entretanto, isso vai resetar no próximo login, para que isso não aconteça, vc deve modificar o `.bashrc` do seu home. Basta acrescentar a linha `umask 0077` no fim do arquivo. E depois rodar o comando `source ~/.bashrc`.