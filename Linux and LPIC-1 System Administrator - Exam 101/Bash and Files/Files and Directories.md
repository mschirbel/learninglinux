# Directories and Files

Para sabermos o que está em um diretório usamos o comando `ls`. Existem diversas variações para o comando com os parâmetros, como por exemplo, `ls -a` para arquivos escondidos, `ls -l` para long list(com as permissões, tamanhos e horas de modificações) e `ls -d` para listar somente os diretórios.

---

O comando `touch` serve para modificar o timestamp de um arquivo. Mas geralmente é usado para criar um novo arquivo.

```
touch new_file
```

---

O comando `cp` é usado para fazer cópias de arquivos. Se usado para outro arquivo, teremos uma cópia. Caso seja para um diretório teremos um novo arquivo no diretório especificado.

Para fazer uma cópia de um diretório, precisamos fazer de uma forma recursiva.

``` 
cp -vR /home/marcelo/meu_dir /opt
```

Caso a origem e o destino sejam iguais o comando `cp` não funciona.

---

O comando `rm` serve para remover arquivos do file system.
Para forçar a operação usamos o parâmetro `-f`. E para remover um diretório, temos que remover de forma recursiva, `rm -r`.

---

O comando `mv` serve para mover arquivos ou diretórios para outros lugares no file system. 
Podemos usar para fazer um rename de um arquivo

```
mv old_name new_name
```

Ou movendo para outro diretório

```
mv file destiny_dir/
```

---

O comando `file` é usado para determinar o tipo de um arquivo.

---

O comando `cd` é usado para trocar de diretórios.

Para voltar para o home do usuário, simplesmente usamos de duas formas:

```
cd
cd ~
```

Existem marcações importantes. O `.` e o `..`, que marcam respectivamente o diretório atual e o anterior.

Isso é super importante para movimentação de arquivos.

---

O comando `mkdir` é usado para criar diretórios a partir de onde estamos.
Para criar um diretório em outro lugar, usamos `mkdir -p`.

```
mkdir cria_aqui/
mkdir -p /usr/local/cria_em_outro_lugar/
mkdir -p cria_uma/cria_duas/cria_tres/
```

Para remover um diretório, além do comando `rm`, podemos usar o comando `rmdir`. Mas também não funciona se existirem arquivos dentro do diretório.

## Compression

O comando `dd` serve para copiar e converter arquivos. Geralmente usamos para fazer arquivos para longos períodos de tempo.

```
dd if=boot.img of=/dev/sdc
```

Com esse comando inserimos a imagem em um pendrive, por exemplo. If é *Input File* e Of é *Output File*.

Ou outro exemplo

```
dd if=/dev/xvda of=/tmp/mbr.img bs=512 count=1
```

Podemos criar um arquivo com tamanhos arbitrários, por exemplo `dd if=/dev/urandom of=file bs=1024k count=10`. Veja o tamanho com `ls -lh file`.

---

O comando `tar` junta arquivos em um único. **Mas não diminui o tamanho dos arquivos**.

```
tar -cf content.tar <ARQUIVO EXISTENTE>
```
`-c` para criar um arquivo
`-f` para dar um nome ao arquivo

Se usarmos o parâmetro `-tf` estamos listando o conteúdo de um arquivo tar.

Para extrair um arquivo tar, usamos o parâmetro `-x`.

---

Para fazer a compressão podemos fazer de algumas formas.

```
tar -czvf content.tar.gz <ARQUIVO(S) EXISTENTE>
```
`-c` para criar um arquivo
`-f` para dar um nome ao arquivo
`-z` para usar o gzip
`-v` para verbose.

*O parâmetro -z deve ser depois do -c*.

Para extrair, usamos o parâmetro `-x`.

---

Temos a convenção de bzips, usando os comando `bzip2` para comprimir e `bunzip2`.
Simplesmente usamos o comando `tar` com o parâmetro `-j`.

---

Com o comando `gzip` podemos usar com parâmetros de `-1` até `-9` para indicar o nível de compressão.

```
gzip -4 arquivo.txt
```

Resultará em um arquivo .gz.
Para extrair, usamos o `gunzip`

---

Existe outro comando que é o `xz`

```
xz file.txt
```

Para fazer a extração

```
unxz file.xz
```