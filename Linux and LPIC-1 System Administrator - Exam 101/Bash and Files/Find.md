# Finding Files

O comando `find` serve para encontrarmos arquivos em um file system.

Por exemplo:

```
find <LOCAL POR ONDE PROCURAMOS>
find .
```

O comando é recursivo, portanto vai encontrar tudo no local onde estamos.
Podemos procurar por arquivos com extensões específicas

```
find . -name *.sh
```

Para procurarmos em locais protegidos, podemos usar `sudo find`

```
sudo find / -name passwd
```

*O comando find é muito pesado no I/O do computador, podendo levar muito tempo pra terminar devido a quantidade de arquivos*.

Podemos pesquisar também por `-ctime` que é o tempo de mudança do arquivo ou por `-atime` que é o tempo de acesso de um arquivo.

Para pesquisar por arquivos vazios usamos o `-empty`.

### Find e exec

Podemos combinar o find para executarmos comando depois.

```
find . -empty -type f -exec rm -f {} \;
```

O `{}` é como se fosse em um laço chamando uma variável. No caso é o resultado do comando find.