# Regex and Globbing

## Globbing

O nome vem de *Global commands*.
Existem diversos globbings no linux, por exemplo:

```
* # zero ou mais caracteres
? # somente um caractere
[abc] # encontra um caractere dentro dos []
[^abc] # encontra qualquer caractere exceto os dentro do []
[0-9] # range de número entre 0 e 9, em qualquer quantidade.
```

Podemos usar o comando `fgrep` que o mesmo que `grep -F`.

## Regex

Expressões regulares são parecidas com Globbing, mas alguns símbolos tem significados diferentes.

Para trabalhar com regex podemos usar o comando `grep`.

`.` para substituir um único caractere
`^` para pesquisar no começo da linha
`$` para pesquisar no final da linha
`[abc]` pesquisar por caracteres específicos
`[^abc]` pesquisar por caracteres exceto os especificados
`*` encontra 0 ou mais caracteres

Ou o comando `egrep` que é o mesmo que `grep -E`.
