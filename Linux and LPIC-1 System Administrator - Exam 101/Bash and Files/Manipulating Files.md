# Edit Files

Podemos fazer o *sort* dentro de um arquivo com o comando `sort`.
O sort é feito em colunas, mesmo que tenhamos números, é feito em **colunas**.

Para fazer um *sort* com números podemos usar o `sort -n`. Assim, consideramos números como um todos.

---

Para imprimir linhas únicas de um arquivo, usamos o comando `uniq`. O comando agrupa as linhas **sequencias** que são iguais.

É possível combinar o sort com o uniq, `sort -u`.

---

Para fazer o *swap* de caracteres, podemos usar o comando `tr`.
Por exemplo:

```
cat test.txt | tr ',' ':'
```

No caso, substituimos o caracter ',' por ':'.

Para deletar um caractere usamos o `tr -d`.

---

Para extrair colunas de um arquivo, podemos usar o comando `cut`.
Por exemplo num arquivo csv, podemos pegar uma coluna específica:

```
cur -d ',' -f 3 test.csv
```
Nesse exemplo, pegamos a terceira coluna(separadas por um ',').

---

Um dos comandos que mais podemos utilizar é o `sed`.

##### Para substituir caracteres

`sed 's/original/nova/g' file.txt`.

Essa é a estrutura para substituir uma palavra por outra nova. O *s* vem de substitute e o *g* de global, para ser no arquivo todo. Isso não muda o arquivo, para isso, temos que usar o *inplace sed* usando o parâmetro *-i*.

`sed -i 's/original/nova/g' file.txt`.

---

Para dividir um arquivo, usamos o comando `split`. Por default, o arquivo novo vai ter até 1000 linhas. Mas podemos usar parâmetro para dividirmos em tamanho, ou menos linhas.

```
split -b 100 #dividir por bytes
```