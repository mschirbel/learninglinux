# Vim

Para aprender vim, use o comando `vimtutor`.

---

Existem alguns modos no Vim:

    - Command Mode(esc) - no qual enviamos comandos para o Vim
    - Inser Mode(i) - indicado no fim da tela, serve para inserir texto
    - Visual Mode(v) - movimentar no arquivo e fazer selecionamentos 

Alguns comandos básicos do Command Mode:

```
:q # sair do Vim
:w # salvar um arquivo
:x # salvar e sair
:X  # criptogravar
dd # apagar uma linha
u # voltar ao estado anterior
dw # apagar uma palavra
gg # volta ao começo do arquivo
```