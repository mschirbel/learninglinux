# Input, Output and Error

## Standard Output

É para onde todas as saídas de comando aparecem no sistema operacional. É mais conhecido como *stdout*.
Podemos fazer o redirect do stdout para algum local, como um arquivo, por exemplo.
Tem o número 1 associado.
No caso, usamos os caracteres `>` ou `>>`.

```
cat file.txt > new_file.txt
```

## Standard Input

Podemos usar entradas de outros comandos ou do teclado para comandos.É mais conhecido como *stdin*.
Tem o número 0 associado.
No caso usamos os caracteres `<` ou `|`.

```
wc < test.sh
find . -name *.sh | grep script?.sh
```

## Standard Error

É a saída de erros típica do sistema. É mais conhecido como *stderr*.
Tem o número 2 associado.

```
internet.sh 2> error.log
```

Fazemos o redirect de tudo que for erro para o arquivo error.log

```
internet.sh 2>&1 | less
```

O stderr e stdout são mandados como stdinn para o comando less.

## Redirect

Podemos usar o comando `tee` para mandar o output de um comando para um arquivo e ainda mostrar na tela

```
ls -d /usr/share/doc/lib[X]* | tee docs.txt
cat docs.txt
```

Podemos usar o comando `tee` em um comando longo e ir *loggando* as fases do comando.

---

Existe também o comando `xargs` para aceitar um input de um stdin de algum comando.

```
find . -mtime +3 | xargs rm -rf
```

Assim, passamos todos os arquivos com mais de 3 dias de criação e todos esses arquivos passarão como input para o comando `rm -rf`.

O `xargs` é mais rápido do que um `-exec` pois cria uma sublista para enviar como input.