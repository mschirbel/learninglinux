# Bash

Shell é o ambiente de linha de comando que usamos no Linux.
O default é o *bash*.

## Bash Environment

### Variables

Temos variáveis que definem o ambiente do bash, as **variáveis de ambiente**.

Por exemplo, `CWD=/home/user/Docs/`.

### Functions

Podemos ter funções para o ambiente

```
function foo(){ 
    echo "bar"
}
```

## Configurar o Ambiente

Podemos ver quais as variáveis estão no nosso ambiente com o comando `env`. Ou as variáveis e as funções, com o comando `set`. Ainda assim, podemos ver as opções do bash com o comando `shopt`, tanto as ativas como as inativas.

Para remover uma função ou variável usamos o comando `unset`. Por exemplo:
```
function foo(){
    echo "bar"
}
foo
unset -f foo
foo
```

É possível exportar variáveis para a atual sessão usando o comando `export`. Caso haja alguma confusão no tipo, podemos usar o comando `type`.

#### Quotes

`echo $PATH` printa o conteúdo da variável PATH
`echo "$PATH"` printa o conteúdo da variável PATH
`echo '$PATH'` printa o texto $PATH

## History

`history`:
Mostra em ordem os comandos utilizados no sistema. O histórico fica no arquivo `.bash_history`. E é referenciado por uma variável que determina quantas linhas esse arquivo contém, essa variável é a **HISTFILESIZE**.

Outro jeito de mostrar é usando o comando `!<NÚMERO DO COMANDO NO HISTORICO>`

## Manual

São documentações de comando, arquivos de configuração entre outros já construídos com o sistema.

É divido em seções:

1. programas executáveis
2. system calls
3. lib calls
4. arquivos especiais
5. convenções e formatos
6. games
7. miscellaneous
8. root adm
9. rotinas de kernel