# View Text Files

## Cat

Podemos usar o comando `cat` para visualizar um arquivo ou *concatenar* arquivos de texto

```
cat test.txt batata.txt
```

Os arquivos continuam separados, mas printamos o conteúdo concatenado.

Existem formas de visualizar o conteúdo de arquivos zipados:

    - zcat: arquivos .gz
    - bzcat: arquivos .bz2
    - xzcat: arquivos .xz

---

Outro comando é o `less` no qual temos um sistema de paginação na leitura do arquivo. Podemos visualizar do mesmo modo com o *vim*. E para sair, basta apertar `q`.

---

Para visualizar partes de um arquivo usamos os comandos `head` e `tail`, respectivamente para visualizar linhas no começo e no fim do arquivo

```
head -10 batata.txt
tail -10 batata.txt
```

## Dados de arquivos

Para ver quantas linhas temos escrita no arquivo usamos o comando `nl` seguido por um arquivo.
Para contar todas as linhas, escritas e não escritas `nl -b`.

Ou podemos contar as linhas e palavaras com o comadno `wc`, wordcount. O primeiro dado é a quantidade de linhas, seguido pelo número de palavras e por último o tamanho do arquivo. Para pegar os valores individualmente, use: `wc -w`, `wc -l` e `wc -c`.

Para ver o conteúdo em formato octal, temos o comando `od`.

## Message Digest

É o valor hash de um arquivo, no qual podemos verificar a autenticidade do arquivo.

Para verificar se o arquivo é o correto use os comandos a seguir.
`md5sum` calcula a soma do hash baseado no algoritmo MD5.
Para testar:

```
md5sum test.txt
md5sum test.txt > test.md5
cat test.md5
md5sum -c test.md5
```

`sha256sum` calcula a hash do arquivo com base no algoritmo SHA-2 com 256 bits.
Para testar:

```
sha256sum test.txt
sha256sum test.txt > test.sha256
sha256sum -c test.sha256
```

`sha512sum` calcula a hash do arquivo com base no algoritmo SHA-2 com 512 bits.
Para testar:

```
sha512sum test.txt
sha512sum test.txt > test.sha512
sha512sum -c test.sha512
```
