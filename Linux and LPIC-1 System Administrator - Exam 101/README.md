Curso da Linux Academy

Preparatório para o exame 101.

## Objetivo: 
    1. arquitetura do sistema
    2. instalar softwares
    3. comandos
    4. file systems

## Tópicos:

Os tópicos da prova são os mesmos do objetivo do curso.
Mas é necessário passar na prova 101-500 e na 102-500.

A prova tem 90min com 60 questões.

## Como estudar

1. Arquitetura do Sistema
    * Pseudo File System
    * Kernel Modules
    * Hardware Status
    * Boot System
    * Upstart
    * Systemd
    * Change Work Environment
    * Main File System
    * LVM
    * GRUB
    * Virtualization and Container
    * Process
2. Libraries
    * Apt
    * Dpkg
    * Yum
    * Rpm
    * Managed Shared Library
3. Bash and Files
    * Bash
    * Files and Directories
    * View Text Files
    * Find
    * Manipulating Files
    * Input, Output and Error
    * Regex in Bash
    * Vim
4. File System
    * File Systems
    * Partitions
    * Mouting
    * Permissions
    * Links
    * Finding Files