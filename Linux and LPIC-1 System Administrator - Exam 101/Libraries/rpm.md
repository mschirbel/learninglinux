# rpm

Temos no *rpm*:
    1. a aplicação
    2. os arquivos de config
    3. como e onde instalar
    4. lista de dependências

É o mesmo que vem no *dpkg*, entretanto, o database do rpm fica em `/var/lib/rpm`.
Caso seja necessário, podemos fazer um rebuild usando o comando `rpm --rebuild`.

Yum cuida das dependências, Rpm não. **isso é importante**.

## Comandos

`rpm -qpi` mostra informações da package
`rpm -ivh` i é para install, v para verbose, h para human readable.
`rpm -U` upgrade de uma package
`rpm -qa` mostra todas as packages instaladas
`rpm -e` apaga uma package

## Acessar os arquivos de uma package

Podemos usar o comando `rpm2cpio`.
Primeiramente fazemos o download da package e usamos o comando:

```
rpm2cpio <NAME>.rpm | cpio -idmv
```

---
