# Yum

Yellowdog Updater Modifier

Ele cuida de dependências para os *.rpm*.

Mesmo princípio do apt para o Debian.

Geralmente é encontrado no RHEL e Fedora.

Talvez será substituido pelo **dnf**.

## Conf

Fica em `/etc/yum.conf` e lê os repos em `/etc/yum.repos.d/` e faz o cache em `/var/cache/yum/`.

Cada repositório será um arquivo. A linha de *baseurl* é a qual indica de onde a package será baixada.

## Commands

`yum update` que lê os repositórios e faz o update das dependências
`yum search` procura em um repositório por uma package
`yum info` informações de uma package
`yum list installed` mostra quais packages estão instaladas
`yum clean all` limpa o cache.
`yum install` para instalar uma package
`yum remove` remove uma package mas não as dependências
`yum autoremove` remove uma package **e as dependências**
`yum whatprovides` procura um arquivo dentro de uma package
`yum reinstall` reinstala uma package
`yumdownloader` baixa uma package. SIM, É JUNTO.

Por exemplo do whatprovides:

```
yum whatprovides */httpd
```