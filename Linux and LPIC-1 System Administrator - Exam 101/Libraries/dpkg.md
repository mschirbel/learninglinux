# DPKG

Debian Package Utility

A principal diferença entre o apt e o dpkg é que este não instala as dependências automaticamente, ou elas já estão no sistema, ou vêm junto no pacote.

## Comandos

Primeiramente precisamos fazer o download da package, que pode ser feito via `apt-get download`, por exemplo.

Para os comandos de dpkg, temos:

`dpkg --info` para mostrar as infos de uma package
`dpkg -i` instala uma package
`dpkg -L` mostra os arquivos que vieram em uma package
`dpkg --status` similar ao info, mas com menos detalhes
`dpkg -r` remove uma package
`dpkg -P` remove uma packate **e todos os arquivos de configuração**
`dpkg-reconfigure` refaz as configurações iniciais de uma package.
`dpkg -S` seguido de uma string, procura por uma package instalada.

É sempre necessário ter acesso *root* para instalar/modificar uma package.
---