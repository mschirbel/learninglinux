# APT

Advanced Package Tool

É a feramenta de controle de packages no Debian. Nos auxilia a instalar uma aplicação **e suas dependências**.

Essa é a principal diferença para o *dpkg*.
Com o apt também podemos desisntalar e fazer upgrades de aplicações.

## How it works

Primeiramente ele lê o `/etc/apt/source.list` e formula tudo o que precisa para terminar a instalação.
Depois ele manda cada uma das packages necessárias, em ordem, para o comando do *dpkg*.

## Comandos

`apt-get udpate` faz o update da lista do cache do apt
`apt-get upgrade` faz o upgrade de uma ou todas as packages
`apt-get install` seguido de uma package válida, instala ela e todas as dependências
`apt-get remove` seguido de uma package que está instalada e remove juntamente com todas as dependências.
`apt-get purge` remove uma package e **todos os arquivos relacionados**.
`apt-get dist-upgrade` faz o upgrade da distribuição
`apt-get download` faz o download de uma package **mas nao instala**.
`apt-cache search` procuramos localmente por alguma package no nosso cache
`apt-cache show` procuramos informação sobre alguma package
`apt-cache showpkg` mostra informações mais técnicas sobre a package e suas dependências

