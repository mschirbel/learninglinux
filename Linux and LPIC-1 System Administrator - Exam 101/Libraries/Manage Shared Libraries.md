# Manage Shared Libraries

Uma shared library são arquivos que contém funcionalidades que aplicações podem usar.

Elas ficam em `/lib/` ou `/usr/lib/`(para 64bit) ou `/usr/local/lib` ou `/usr/share`.
Tem a extensão *.so*.

Para descobrir, temos uma variável de ambiente chamada de `$LD_LIBRARY_PATH`

Existem dois tipos:
1. dinâmicas, que terminam em *.so*
2. static link, que terminam em *.a*

## Commands

Podemos ver se algum comando do SO usa alguma shared library usando o comando `ldd`. Por exemplo:

```
ldd /bin/cp
ldd /bin/hostname
```

---

O comando `ldconfig` cria um cache das libraries que vc mais usou no sistema:

```
sudo ldconfig
cat /etc/ld.so.conf
ls /etc/ld.so.conf.d/
```