# Kernel Modules

É o framework principal do SO. No caso é o Linux. Para a maioria das distribuições temos o GNU/Linux. Linux é o Kernel e GNU são pacotes construidos para interagir com o Kernel.

O Kernel tem a função de interagir com o hardware de forma monolítica, isso significa que ele cuida de tudo que é interações com hardware e uso de memória

Isso permite que existam módulos para novas funções. Ou seja, não precisamos de um reboot para instalar um driver, por exemplo.

Para saber as informações do Kernel usamos o comando `uname`. 

`uname -r` é a release
`uname -m` é o tipo
`uname -a` vem todas as informações

Para vermos os módulos com o comando `lsmod`. Assim vemos todos os módulos em uso no nosso SO, bem como a memória e as dependências.

Para vermos informações sobre um módulo específico usamos o comando `modinfo`. Como por exemplo `modinfo floppy`.

Para fazermos o load e unload de um módulo usando o comando `modprobe`. Podemos testar com o módulo floppy, como por exemplo

```
modprobe -r floppy
lsmod|grep floppy
modprobe floppy
lsmod|grep floppy
```

Quando fazemos o load de um módulo, ele faz o load das dependências também.