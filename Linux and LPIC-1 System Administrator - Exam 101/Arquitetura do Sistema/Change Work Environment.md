# Change Working Environment

## Runlevels

Os runlevels que existem são os seguintes:

![](../media/runlevels.PNG)

Podemos ver qual runlevel estamos usando com o comando `runlevel`.
O output do comando vai ser "N 5", se estivermos em um SO com interface gráfica. O N significa que não houve troca de runlevel antes

Para fazer uma troca, usamos o comando `telinit`, seguido de algum número para indicar o runlevel desejado. Após fazer isso, podemos ver que o output do comando `runlevel` foi alterado.

**É necessário ser root para fazer essas alterações**.

Para ver os runlevels no sistema:

```
cat /etc/inittab
```

E aqui podemos ver qual o default do sistema.

## Targets

Vamos ver o target.unit

Que server para fazer uma sincronização entre outros units files quando o sistema inicia.
Temos alguns targets que server como apoio:

    - multi-user.target = similar ao runlevel 3
    - graphical.target = similar ao runlevel 5
    - rescue.target = sistema básico com FS e um shell
    - basic.target = sistem básico para ser usado por outro processo
    - sysinit.target = inicialização do sistema

Podemos ver um exemplo:

```
systemctl cat graphical.target
```

Para ver os target unit files:

```
systemctl list-unit-files -t target # todos
systemctl list-unit -t target # somente os ativos
systemctl get-default # vemos qual o target default
```