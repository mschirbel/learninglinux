# Process

## Monitoring

Para monitorar por quanto tempo o sistema está online usamos o comando `uptime`.
Entretanto o comando mostra mais alguns dados:
    - uptime
    - usuários online
    - cpu no último minuto
    - cpu nos últimos 5 minutos
    - cpu nos últimos 10 minutos

---

Para monitorar a quantidade de memória usada no sistema usamos o comando `free`.
Aqui podemos ver a memória RAM e a memória swap.

---

Para informações sobre processos usamos o comando `pgrep`.
Com esse comando pesquisamos pelo nome do processo.

Para ver todas as informações use `pgrep -a`.

Ou processos por usuários `pgrep -u <USUÁRIO>`.

---

Para encerrar um processo, podemos usar dois comandos:

`kill` no qual mandamos um sinal para um **PID**.
`pkill` no qual mandamos um sinal para um **nome de processo**.

---

Podemos matar mais de um processo ao mesmo tempo usando o comando `killall`, que é baseado em um argumento

```
sudo killall java
sudo killall httpd
```

---

Para monitorarmos o output de um comando, usamos o `watch`.

```
watch -n5 kubectl get pods -n kube-system
```

---

Outro comando útil é o `screen`. Com esse comando podemos rodar outros comandos em uma sessão isolada que funciona independentemente de usuários.

```
screen # iniciar uma screen
<ctrl + A + D> # sair do screen
screen -r <NÚMERO DA SCREEN># voltar para a screen
screen -ls # listar screen existentes
```

---

Um comando parecido com o screen é o `tmux`. É a mesma função do screen mas com algumas coisas extras.

```
tmux # iniciar
<ctrl + B + D> # sair
tmux ls # listar
tmux attach-session -t <NUMERO DA SESSÃO>
exit # sair do tmux
```

## Manter processos rodando

Podemos usar o comando `nohup`. Assim mantemos o comando(SIGNAL 1) e mandamos para o background

```
nohup ping www.google.com &
```

Para listar, usamos o comando `jobs`. Todas as logs ficam no arquivo `nohup.out`.

Para trazer o comando do background para o foreground usamos o comando `fg <JOB NUMBER>`
Para trazer o comando do foreground para o background usamos o comando `bg <JOB NUMBER>`

## Prioridade de Processos

Existe uma ordem de prioridade para uso de CPU. Assim temos uma ordem para definir quanto tempo de CPU é garantido a um processo.

A ordem pode ir de -20(maior ordem de prioridade) até 19(menor possível). Sendo 0 o valor default.
Podemos diminuir(aumentar o número) a prioridade mas **somente o root** pode aumentar a prioridade(diminuir o número).

Para alterar essa prioridade usamos dois comandos: `nice` e `renice`.

A diferença é que o `nice` é usado para iniciar comandos. O `renice` serve para comandos que já estão rodando.

```
nice -n -5 watch -n3 date
ps -o pid,nice,cmd,user
sudo renice -n -1 <PID>
```