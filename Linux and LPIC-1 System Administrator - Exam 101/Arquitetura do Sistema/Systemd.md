# Systemd

É o mais usado atualmente para fazer o boot de um sistem linux. Com o systemd removemos a necessidade do boot usar shellscripts, pois isso fazia necessidades de interpretadores de bash, ou seja, mais lento.

Para isso, eles replicaram os códigos em shellscript para C, assim fica muito mais rápido.

Ainda podemos usar os antigos init scripts, mas eles não são mais tão eficientes.

## Unit Files

Systemd usa Unit Files para fazer o boot. Podemos encontrá-los em:

    - `/usr/lib/systemd/system` - não devemos editar
    - `/etc/systemd/system/` - podemos editar
    - `/run/systemd/system/` - podemos editar

Para ver todos os unit files usamos o comando `systemctl list-unit-files`.

O formato padrão é mais ou menos assim:

```
[Unit]
Description=Multi-User System
Documentation=man:systemd.special(7)
Requires=basic.target
Conflicts=rescue.services rescue.target
After=basic.target rescue.service rescue.target
man 5 systemd.unit
```

Podemos ver qualquer unit file usando o comando `systemctl cat nome.unit`, sendo nome o nome do unit file que vc deseja ver. Por exemplo `systemctl cat httpd.service`.

## Boot
O Kernel ainda só procura por `/sbin/init`, logo, o que foi feito é um link simbólico para `/lib/systemd/systemd`.