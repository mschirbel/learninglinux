# Investigating Hardware

Existe um serviço que é o `udev`, Unix Device Manager. Por exemplo, quando um hardware é conectado, temos que as informações serão levadas pelo *D-Bus* para o `/dev/`. Nesse file system temos todos os devices conectados no sistema.

Para acessarmos os blocos de devices, usamos o comando `lsblk` para, por meio do *D-Bus* acessarmos o conteúdo do `/dev/`.

Podemos ver as informações de devices da cpu:

```
ls /dev/cpu
```

Assim vemos todos os processadores, e dentro de cada um deles temos as informações da cpu de cada um dos cores.

```
ls /dev/cpu/0
```

## Lendo as informações do /dev

Temos alguns comandos para facilitar nossa vida:

```
# Para vermos as informações de PCI
lspci

# Para referenciar os kernel modules
lspci -v

# Para vermos as informações de USB, tipos de portas e fabricantes
lsusb

# Para as informações em formato de árvore correlacionada
lsusb -t

# Informações dos processadores
lscpu

# Informações de todos os blocos de devices
lsblk
```

