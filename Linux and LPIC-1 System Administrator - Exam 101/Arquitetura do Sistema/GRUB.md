# Grub

## Legacy Grub

Quando o computador percebe que todos os componentes estão prontos para ligar, a BIOS chama o Boot Loader, que está a imagem do boot, com 512bytes. A imagem do boot chama a imagem do core que chama as partições do grub. A partir desse diretório pocura-se o `grub.conf` e o `device.map`.

`grub.conf` é o arquivo que contém informações sobre o boot do sistema, como timeout, drive e versão de Kernel.
`device.map` é o arquivo para indicar em qual drive está o Kernel.

E com isso temos a possibilidade de bootarmos diversos Kernels com o mesmo hardware e mesmo sistema operacional. E tudo isso fica no `grub.conf`.

#### Install Legacy Grub

```
grub install '(hd0)'
```

Pois o grub deve ser instalado no `/boot`. Que pode ser encontrado em `findmnt /boot`.

Para entrar no grub shell, usamos o comando `grub`.
Depois usamos o comando `find /grub/stage1`.

## New Grub - Grub2

Antigamente tinhamos o **MBR**(Master Boot Record), que suportava até 23 partitions e cada uma com o tamanho máximo de 2TB.

Agora, usamos o **GPT**(Guid Partition Table) que suporta até 128 partitions e cada uma com o tamanho máximo de 1 ZettaByte.

Mas para usarmos o **GPT** precisamos o UEFI(Unified Extensible Firmware Interface):
    - replace da bios(mas age como ela)
    - precisa de um OS de 64bits
    - nao deixa que OS estranhos façam boot(por isso tem que desabilitar caso queira trocar)

![](../media/grub2.PNG)

Assim como no Legacy, temos a imagem do boot com 512bytes, essa agora chama o GPT Header que vai ler de um disco do tipo GPT.
A partir daí temos dentro do header o array com as partitions. Na primeira partition teremos a imagem core que dessa vez vai chamar o `/boot/efi`(que deve ser um vfat ou FAT32), apra assim chamar o `/boot/grub2` com as configurações de boot.

### Modify GRUB2

`grub2-editenv list` para ver o arquivo de configuração, que está em `/boot/grub/grubenv`. E nunca editamos mais os arquivos diretamente, usamos comandos que fazem isso indiretamente para nós.

O arquivo que modificamos mesmo é o `/etc/default/grub`. Depois de modificar o arquivo, usamos o comando `grub2-mkconfig` Esse comando cria os arquivos de configurações novos baseado no arquivo que vc editou. Ou o `update-grub` para Debian.

## Boot Loader on Grub Legacy

Para interagir no boot loader temos algumas opções:

1. setas do teclado, para nos movermos entre as opções
2. esc, para sair do menu
3. A, para fazer um *append* no kernel boot, por exemplo, inserir qual versão de Kernel vamos usar.
4. C, para abrir a *command line*.

Para reinstalar o grub, abrimos o grubshell e podemos usar o comando `setup` ou `install`, mas o último é bem complicado.
Selecionamos qual o device que queremos o grub, como *hb0*.

## Boot Loader on Grub 2

Para interagir nesse temos outras opções:

1. E, para editar um item do menu
2. esc, para sair do menu
3. F10, escolhe a linha modificada como boot
4. C, para abrir a *command line*.

Assim que editamos uma linha, temos que usar o F10 para fazer o boot.
Dentro do grubshell, podemos usar o comando `ls` para ver quais os devices listados.

Agora teremos um modo de instalar o grub, assim quando dentro do grubshell:

```
ls
ls (hd0,1)/
set root=(hd0,1)
linux /boot/<KERNEL VERSION> root=/dev/<DISK># é possível usar tab aqui
initrd /boot/initrd.img-4.13.0-43-generic 
boot
```