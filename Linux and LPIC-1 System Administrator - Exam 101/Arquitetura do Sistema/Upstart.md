# Upstart

Foi usando primeiramente no Ubuntu 6.0.
O upstart diminuia o tempo de boot time pois fazia o start dos serviços de forma assíncrona.

Não somente monitora se um serviço iniciou ou não, mas pode fazer stop e start deles em *real-time*.

---

O boot start do Upstart é diferente do Init:

![](../media/init_vs_upstart.PNG)

É algo bem simplificado para mostrar como os dois são diferentes.

## Diferenças

Init é estático, e não responde a mudanças do sistema
Upstart é dinâmico, pois responde a mudanças no sistema

Uma mudança no sistema do Linux é um **event**. Um **event** cria um **job** e um job pode ser:
    - tasks
    - services

O upstart só para um serviço se o administrar mandar ou se algum evento pedir.

### Jobs

Existe um flow para um job:

![](../media/job_flow.PNG)

O respawn de um job acontece quando por alguma razão durante o *running* ele parou de funcionar. O upstart tenta reiniciar o job por 10x durante 5s de intervalo antes de matá-lo.

