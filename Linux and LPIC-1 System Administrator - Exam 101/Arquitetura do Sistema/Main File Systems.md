# Main File System Locations

Devemos saber alguns:

    - / - root directory
    - /var - conteúdo dinâmico, como logs e websites
    - /home - onde os arquivos pessoais são armazenados
    - /boot - onde o Kernel está e tudo que é preciso pra ligar o sistema
    - /opt - geralmente usado para terceiros e softwares externos

## Swap Space

Storage temporário que age como RAM. Quando a RAM está cheia, ela usa a swap.
Pode ser uma partition ou um file.

File é muito mais devagar que uma partition.
No file(assim como a paginação do windows) estamos escrevendo em um arquivo, que é salvo no HD. Enquanto que na partition estamos escrevendo diretamente no HD.

#### Swap size

O tamanho da swap deve ser no mínimo 50% da RAM.

Para ver o tamanho da swap podemos usar o comando `swapon --summary`, ou na relação em `free -m`.

## Partitions e Mount Points

Vamos supor que vc tenha um único device, o `/dev/sda`, o primeiro drive que o Kernel reconhece.
Então podemos ter uma partição desse disco, seja a `/dev/sda1`, ou podemos ter `/dev/sda2` e também outras. Cada uma com uma funcionalidade.

Um mount point é alocar uma partição a um diretório do sistema.

Para verificar quantas partições e quantos mount points temos usamos o comando `mount`.

Para verificar os block devices usamos o comando `lsblk`, que são blocos de dados aglomerados em discos+partições.

Para vermos as partitions de um disco específico usamos o comando `fdisk -l /dev/DISKNAME`(sem o número).

### Nomenclatura

Quando temos um `/dev/sda1`, o 'a' significa que é o primeiro disco lido pelo sistema. E o 1 significa que é a primeira partição sobre aquele disco.
E também sabemos que é um disco físico pois temos o 'd'.
Caso seja um disco virtual, como numa máquina virtual, teremos o 'v'.