# Boot System

Depois de um computador iniciar, a Bios verifica com a placa mãe se tudo está OK.

Depois é feito uma chamada ao HDD pelo *boot sector* para termos as informações necessárias.
Depois é feito o load do Kernel e depois o Inital RAM Disk.

A partir daí montamos o FS e o Sistema de Inicialização.
O Inital RAM Disk é desligado e o Sistema de inicialização começa a iniciar os serviços e daemons necessários.

## Temos logs da inicialização

Essas logs são voláteis e são geradas pelo ![*Kernel Ring Buffer*](https://unix.stackexchange.com/questions/198178/what-are-the-concepts-of-kernel-ring-buffer-user-level-log-level).

Para acessá-las usamos o comando `dmesg`. Temos detalhes dos devices de hardware e controle de memória.

Nos Unix mais novos, temos o systemd que faz a inicialização, com isso temos o `journalctl -k`. Com isso veremos as mensagens de Kernel.

## Init 

É uma abreviação de initialization e temos uma sequencia para inicialização de serviços, isso traz um problema enorme caso algum serviço trave, todo o sistema trava.

O primeiro programa que é inicializado é o `/sbin/init`. Que por sua vez lê o `/etc/inittab`, um arquivo de configurações, para saber qual *runlevel* o sistema tem que iniciar.

### Runlevel

    - 0: halt(shutdown)
    - 1: single user mode - só temos o root
    - 2: multi user mode(no network)
    - 3: multi user mode(with network)
    - 4: unused
    - 5: multi user + network + graphical desktop
    - 6: reboot

Um exemplo de configuração seria assim, sem o processo:

```
# /etc/inittab
# <id>:<runlevel>:<action>:<process>
id:3:initdefault
```

Ou outro exemplo, sem o runlevel

```
# /etc/inittab
# <id>:<runlevel>:<action>:<process>
si::sysinit:/etc/rc.d/rc.sysinit
```

Outro exemplo

```
# /etc/inittab
# <id>:<runlevel>:<action>:<process>
10:0:wait:/etc/rc.d/rc 0
```

Os scripts para inicialização podem estar em:

    - red hat: `/etc/rc.d/`
    - debian: `/etc/inid.d`

rc significa *run commands*.
E o número no lado de cada um é o *runlevel* de cada um deles.

Se olharmos o conteúdo de `/etc/rc.d/rc3.d/` que é a pasta para alguns dos scripts de inicialização em runlevel 3, veremos que os arquivos segue um padrão

```
<LETRA>:<NUMERO>:<NOME>
```

As letras podem ser **K** ou **S**. K para *kill* e S para *start*. Assim o sistema sabe quais processos deve matar e quais devem ser iniciados

Os números indicam a ordem que deve ser seguida no runlevel.

## Upstart

Para fazer o restart de um sistema linux, usamos o comando `telinit 6`. Ou simplesmente o comando `reboot`. Ainda é possível usar o comando `shutdown -r now`, com o parâmetro de reboot e com um delta time para o restart.
Se vc usar o *systemd* é possível usar o comando `systemctl isolate reboot.target`.

## Power down

Para enviar mensagens a outros usuários usamos o comando `wall`. Precione Ctrl+D para enviar a mensagem.

Para realizar o power down use o comando `poweroff` ou até mesmo o `telinit 0`. Se quiser um shutdown com delay, `shutdown -h +1`. O parâmetro -h é para power down, logo seguido de 1 minuto.
Se vc usar o *systemd* é possível usar o comando `systemctl isolate poweroff.target`.

### ACPID

Advanced Configuration and Power Interface. É um sistema que registra eventos de hardware sobre desligar ou reiniciar o computador.

Podemos ver isso em `/etc/acpi/` e depois dentro de `events`.

```
cat /etc/acpi/events/power.conf
```

