# Virtualization

Uma VM são emulações de um sistema computacional.
No qual, é possível instalar diferentes sistemas operacionais, dividindo o mesmo hardware.

Para isso, usamos um Hypervisor. Como por exemplo o KVM, VMWare, VirtualBox, Hyper-V

## Tipos de Virtualização

1. Full Virtualization - o sistema guest não sabe que é uma VM
2. Paravirtualization - o sistema guest sabe que é uma VM e usa drivers próprios(geralmente tem melhor performance)

---

As vantagens de usar uma VM é que podem ser clonadas e rapidamente colocadas em outros sistemas.
VM são extensamente usadas nos cloud providers, pois assim tudo é rapidamente provisionado.

# Container

São packages, libs e ou aplicações totalmente isoladas e independentes.

Existem dois tipos: Machine Container e Application Container

1. Machine: divide o kernel e os arquivos de sistema com o host
2. Application: dividide tudo menos os arquivos da aplicação

Container são mais rápidos de instanciar do que uma VM, é um novo jeito de encarar as arquiteturas modernas.
Entretanto, as VMs tem uma divisão entre os sistemas muito mais forte que os containers.