# LVM

Logical Volume Manager.
Auxilia a criação de grupos de discos ou partições para que possamos juntá-los em um único file system.

Podemos usar em qualquer mount point, **exceto /boot**.

Com isso, temos flexibilidade para fazer um resizing e também podemos tirar snapshots mais facilmente.

## Architecture

Temos os nossos volumes físicos, por exemplo `/dev/sda`, `/dev/sdb`, `/dev/sdc`.
Juntamos eles em um Volume Group chamado de `vg_base`.
Acima desse VG podemos colocar logical volumes para indicar onde vamos separar os mount points do file system:

![](../media/LVM.PNG)

## Comandos

`pvs` para os volumes físicos
`vgs` para os Volume Groups
`lvs` para os Logical Volumes.