# Pseudo File System

Um file system método de distribuir arquivos e pastas em um disco físico.
No linux, qualquer coisa é um arquivo, seja um mouse, um HD, ou uma pasta

Mas um pseudo file system não é real, ele somente é criado quando o PC liga e fica na memória RAM.

Existem diversos que são usados, mas veremos 2:

    - /proc
    - /sys

## /proc

Diretório com informações sobre processos e informações do computador

Podemos entrar no diretório no primeiro `cd /proc` e usamos o `ls` para ver o que temos ali dentro.
Perceba que existem diversas pastas com números, isso são processos que estão rodando no pc, associados com um PID

Temos outras informações, como cpu, memória, partições, versões, uptime e etc.

Para ver as informações de cpu `cat /proc/cpuinfo`. Temos informações do processador que nosso computador usa. Esse arquivo é criado em real-time, quando o pc está ligado.

Podemos ver o diretório do primeiro processo `cd /proc/1/`. Nesse diretório temos informações do processo específico.

## /sys

Nesse diretório temos informações sobre o kernel e o hardware conectado ao computador.

Podemos ver o diretório `fs` no qual temos informações sobre como os arquivos são gravados no computador.
Dentro dele, temos dois outros diretórios, o `stats` que mostra dados sobre o estado corrente do file system e o `xvda1`, que é a partição do disco sobre a qual o FS está.

